import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ResultComponent } from './result/result.component';
import { GameComponent } from './game/game.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/game',
    pathMatch: 'full'
  },
  {
    path: 'game',
    component: GameComponent
  },
  {
    path: 'results',
    component: ResultComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);
