import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable'
import { startWith } from 'rxjs/operators/startWith'
import { map } from 'rxjs/operators/map';
import { ServerCommunication } from '../communication.service'

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  public timer: number = 0;
  public chosen: Array<string> = [];
  public data: Object = {
    vreme: 0,
    ponudjene: []
  };

  public city: string = "";

  public myControl: FormControl = new FormControl();

  private filteredOptions: Observable<string[]>;

  constructor(private _server: ServerCommunication) {}


  ngOnInit(){
    this._server.getJSON('podaci.json').subscribe(
      res => {
        this.data = res;
        this.timer = this.data.vreme;
        this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter(val))
          );
      },
      err => {
        console.log(err);
      }
    );

    const time = setInterval(() => {
      this.timer--;
      if(this.timer < 1) {
        clearInterval(time);
        this.finishGame();
      }
    }, 1000)
  }

  filter(val: string): string[] {
    return this.data.ponudjene.filter(option =>
      option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  choseCity() {
    if(!this.city || this.city == '') {
      return;
    }
    this.chosen.push(this.city);
    this.city = '';
  }

  deleteCity(val) {
    this.chosen.splice(val, 1);
  }

  finishGame() {
    location.href = '/results?selected=' + this.chosen;
  }

  convertTime(val) {
    if(val < 60) {
      return val + "s";
    }
    return Math.floor(val / 60) + "m " + (val % 60) + "s";
  }

}
