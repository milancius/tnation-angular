import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routing } from './app.routing'
import 'hammerjs';


import { AppComponent } from './app.component';
import { ServerCommunication } from './communication.service';
import { ResultComponent } from './result/result.component';
import { GameComponent } from './game/game.component'


@NgModule({
  declarations: [
    AppComponent,
    ResultComponent,
    GameComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [
    ServerCommunication
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
