/**
 * Created by milan on 26.1.2017..
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest, HttpEvent } from '@angular/common/http';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/throw';

@Injectable()
export class ServerCommunication {

    /*******************************************
     *                                         *
     *            PUBLIC VARIABLES             *
     *                                         *
     *******************************************/


    /*******************************************
     *                                         *
     *           PRIVATE VARIABLES             *
     *                                         *
     *******************************************/

    private _localPath = '/assets/';

    constructor(private _http: HttpClient) {}

    /*******************************************
     *                                         *
     *             PUBLIC METHODS              *
     *                                         *
     *******************************************/

    getJSON(path: string) {
        return this._http.get(this._localPath + path);
    }
}
