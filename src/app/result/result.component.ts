import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServerCommunication } from '../communication.service'

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  private answers: Array<string> = [];
  private correctPercentage: number = 0;
  private animatedPercentage: number = 0;

  constructor(private _route: ActivatedRoute, private _server: ServerCommunication) { }

  ngOnInit() {
    this._route.queryParams.subscribe(
      params => {
        this.answers = params.selected.split(',');

        this._server.getJSON('podaci.json').subscribe(
          res => {
            for(let i of this.answers) {
              if(res.tacno.indexOf(i) != -1) {
                this.correctPercentage += Math.round(100 / this.answers.length);
                break;
              }
            }

            setTimeout(() => {
              document.getElementById('chart').style.width = this.correctPercentage + "%";

              const timer = setInterval(() => {
                this.animatedPercentage++;
                if(this.animatedPercentage >= this.correctPercentage) {
                  this.animatedPercentage > this.correctPercentage ? this.animatedPercentage = this.correctPercentage : null;
                  clearInterval(timer);
                }
              }, 4500 / this.correctPercentage);
              
            }, 1000);
          },
          err => console.log(err);
        );
      }
    );
  }

}
